package org.tinygroup.bizframe.service.inter;

import org.tinygroup.bizframe.common.dto.PageRequest;
import org.tinygroup.bizframe.common.dto.PageResponse;
import org.tinygroup.bizframe.service.inter.dto.SysUserDto;
import org.tinygroup.bizframe.service.inter.dto.SysUserLoginDto;

/**
 * 用户登录状态
 * Created by Mr.wang on 2016/7/14.
 */
public interface SysUserLoginService {
    SysUserLoginDto addSysUserLogin(SysUserLoginDto sysUserLoginDto);

    int updateSysLogin(SysUserLoginDto sysUserLoginDto);

    int deleteSysUserLogin(String depCode);

    PageResponse getSysUserLoginPager(PageRequest pageRequest, SysUserLoginDto sysUserLoginDto);

    SysUserLoginDto getSysUserLoginByUser(SysUserDto sysUserDto);
}
