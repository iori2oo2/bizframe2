package org.tinygroup.bizframe.business.inter;

import org.tinygroup.dict.Dict;
import org.tinygroup.dict.DictManager;

import java.util.List;

/**
 * Created by wangwy on 2016/11/30.
 */
public interface DictDataLoaderBusiness {
    List<Dict> loadDict();
}
