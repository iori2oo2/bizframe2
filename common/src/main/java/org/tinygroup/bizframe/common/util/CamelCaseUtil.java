package org.tinygroup.bizframe.common.util;

import org.tinygroup.commons.namestrategy.NameStrategy;
import org.tinygroup.commons.namestrategy.impl.CamelCaseStrategy;

/**
 * 驼峰工具
 * Created by Mr. wang on 2016/11/7.
 */
public class CamelCaseUtil {
    private static NameStrategy nameStrategy = new CamelCaseStrategy();
    public static String getFieldName(String propertyName){
        return nameStrategy.getFieldName(propertyName);
    }
}
