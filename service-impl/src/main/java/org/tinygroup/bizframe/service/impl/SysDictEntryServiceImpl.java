package org.tinygroup.bizframe.service.impl;

import org.tinygroup.bizframe.basedao.util.PageResponseAdapter;
import org.tinygroup.bizframe.business.inter.SysDictEntryBusiness;
import org.tinygroup.bizframe.common.dto.PageRequest;
import org.tinygroup.bizframe.common.dto.PageResponse;
import org.tinygroup.bizframe.common.util.BeanUtil;
import org.tinygroup.bizframe.common.util.CamelCaseUtil;
import org.tinygroup.bizframe.dao.inter.constant.TsysDictEntryTable;
import org.tinygroup.bizframe.dao.inter.pojo.TsysDictEntry;
import org.tinygroup.bizframe.service.inter.SysDictEntryService;
import org.tinygroup.bizframe.service.inter.dto.SysDictEntryDto;
import org.tinygroup.commons.tools.StringUtil;
import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.tinysqldsl.Pager;

/**
 * Created by Mr.wang on 2016/7/21.
 */
public class SysDictEntryServiceImpl implements SysDictEntryService{
	private SysDictEntryBusiness sysDictEntryBusiness;

    public SysDictEntryBusiness getSysDictEntryBusiness() {
		return sysDictEntryBusiness;
	}

	public void setSysDictEntryBusiness(SysDictEntryBusiness sysDictEntryBusiness) {
		this.sysDictEntryBusiness = sysDictEntryBusiness;
	}

	public SysDictEntryDto getDictEntry(Integer dictEntryId) {
		TsysDictEntry DictEntry = sysDictEntryBusiness.getById(dictEntryId);
		SysDictEntryDto DictEntryDto = new SysDictEntryDto();
        BeanUtil.copyProperties(DictEntryDto,DictEntry);
        return DictEntryDto;
    }

    public SysDictEntryDto addDictEntry(SysDictEntryDto dictEntryDto) {
        TsysDictEntry dictEntry = BeanUtil.copyProperties(TsysDictEntry.class,dictEntryDto);
        return BeanUtil.copyProperties(SysDictEntryDto.class,sysDictEntryBusiness.add(dictEntry));
    }

    public int updateDictEntry(SysDictEntryDto dictEntryDto) {
        TsysDictEntry dictEntry = BeanUtil.copyProperties(TsysDictEntry.class,dictEntryDto);
        return sysDictEntryBusiness.update(dictEntry);
    }

    public void deleteDictEntrys(Integer[] ids) {
        sysDictEntryBusiness.deleteByKeys(ids);
    }

    public PageResponse getDictEntryPager(PageRequest pageRequest, SysDictEntryDto dictEntryDto) {
        TsysDictEntry dictEntry = BeanUtil.copyProperties(TsysDictEntry.class,dictEntryDto);

        String sortField = pageRequest.getSort();
        if(StringUtil.isEmpty(sortField)){
            sortField= CamelCaseUtil.getFieldName("id");
        }
        String orderByField=CamelCaseUtil.getFieldName(sortField);
        OrderBy orderBy=new OrderBy(orderByField,"asc".equalsIgnoreCase(pageRequest.getOrder()));

        Pager<TsysDictEntry> dictEntryPager = sysDictEntryBusiness.getPager(pageRequest.getStart(),pageRequest.getPageSize(),dictEntry,orderBy);
        return PageResponseAdapter.transform(dictEntryPager);
    }

    public boolean checkDictEntryExists(SysDictEntryDto dictEntryDto) {
        TsysDictEntry dictEntry = BeanUtil.copyProperties(TsysDictEntry.class,dictEntryDto);
        return sysDictEntryBusiness.checkExists(dictEntry);
    }

}
