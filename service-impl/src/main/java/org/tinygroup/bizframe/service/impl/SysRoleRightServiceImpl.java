package org.tinygroup.bizframe.service.impl;

import org.tinygroup.bizframe.basedao.util.PageResponseAdapter;
import org.tinygroup.bizframe.business.inter.SysRoleRightBusiness;
import org.tinygroup.bizframe.common.dto.PageRequest;
import org.tinygroup.bizframe.common.dto.PageResponse;
import org.tinygroup.bizframe.common.util.BeanUtil;
import org.tinygroup.bizframe.common.util.CamelCaseUtil;
import org.tinygroup.bizframe.ext.dao.inter.constant.TsysRoleRightTable;
import org.tinygroup.bizframe.ext.dao.inter.pojo.TsysRoleRight;
import org.tinygroup.bizframe.service.inter.SysRoleRightService;
import org.tinygroup.bizframe.service.inter.dto.SysRoleRightDto;
import org.tinygroup.commons.tools.StringUtil;
import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.tinysqldsl.Pager;

public class SysRoleRightServiceImpl implements SysRoleRightService {
	
	private SysRoleRightBusiness sysRoleRightBusiness;
	
	public SysRoleRightBusiness getSysRoleRightBusiness() {
		return sysRoleRightBusiness;
	}

	public void setSysRoleRightBusiness(SysRoleRightBusiness sysRoleRightBusiness) {
		this.sysRoleRightBusiness = sysRoleRightBusiness;
	}

	public SysRoleRightDto getSysRoleRight(Integer id) {
		TsysRoleRight sysRoleRight = sysRoleRightBusiness.getById(id);
		SysRoleRightDto sysRoleRightDto = new SysRoleRightDto();
		BeanUtil.copyProperties(sysRoleRightDto, sysRoleRight);
		return sysRoleRightDto;
	}

	public SysRoleRightDto addSysRoleRight(SysRoleRightDto sysRoleRightDto) {
		TsysRoleRight sysRoleRight = BeanUtil.copyProperties(TsysRoleRight.class, sysRoleRightDto);
		return BeanUtil.copyProperties(SysRoleRightDto.class, sysRoleRightBusiness.add(sysRoleRight));
	}

	public int updateSysRoleRight(SysRoleRightDto sysRoleRightDto) {
		TsysRoleRight sysRoleRight = BeanUtil.copyProperties(TsysRoleRight.class, sysRoleRightDto);
        return sysRoleRightBusiness.update(sysRoleRight);
	}

	public void deleteSysRoleRight(Integer[] ids) {
		sysRoleRightBusiness.deleteByKeys(ids);
	}

	public PageResponse getSysRoleRightPager(PageRequest pageRequest,
			SysRoleRightDto sysRoleRightDto) {
		TsysRoleRight sysRoleRight = BeanUtil.copyProperties(TsysRoleRight.class, sysRoleRightDto);

		String sortField = pageRequest.getSort();
		if(StringUtil.isEmpty(sortField)){
			sortField= CamelCaseUtil.getFieldName("menuId");
		}
		String orderByField= CamelCaseUtil.getFieldName(sortField);
		OrderBy orderBy=new OrderBy(orderByField,"asc".equalsIgnoreCase(pageRequest.getOrder()));

		Pager<TsysRoleRight> pager = sysRoleRightBusiness.getPager(pageRequest.getStart(),pageRequest.getPageSize(),sysRoleRight,orderBy);
		return PageResponseAdapter.transform(pager);
	}

	public boolean checkSysRoleRightExists(SysRoleRightDto sysRoleRightDto) {
		TsysRoleRight sysRoleRight = BeanUtil.copyProperties(TsysRoleRight.class, sysRoleRightDto);
		return sysRoleRightBusiness.checkExists(sysRoleRight );
	}


}
