package org.tinygroup.bizframe.service.impl;

import java.util.List;

import org.tinygroup.bizframe.basedao.util.PageResponseAdapter;
import org.tinygroup.bizframe.business.inter.SysOfficeBusiness;
import org.tinygroup.bizframe.common.dto.PageRequest;
import org.tinygroup.bizframe.common.dto.PageResponse;
import org.tinygroup.bizframe.common.util.BeanUtil;
import org.tinygroup.bizframe.common.util.CamelCaseUtil;
import org.tinygroup.bizframe.dao.inter.constant.TsysOfficeTable;
import org.tinygroup.bizframe.dao.inter.pojo.TreeData;
import org.tinygroup.bizframe.dao.inter.pojo.TsysDep;
import org.tinygroup.bizframe.dao.inter.pojo.TsysOffice;
import org.tinygroup.bizframe.service.inter.SysOfficeService;
import org.tinygroup.bizframe.service.inter.dto.SysOfficeDto;
import org.tinygroup.bizframe.service.inter.dto.TreeDto;
import org.tinygroup.commons.tools.StringUtil;
import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.tinysqldsl.Pager;

/**
 * 岗位信息 service-impl
 *
 * @author Mr.wang
 * @date 2016/07/12
 */
public class SysOfficeServiceImpl implements SysOfficeService {

    private SysOfficeBusiness sysOfficeBusiness;

    public SysOfficeBusiness getSysOfficeBusiness() {
        return sysOfficeBusiness;
    }

    public void setSysOfficeBusiness(SysOfficeBusiness sysOfficeBusiness) {
        this.sysOfficeBusiness = sysOfficeBusiness;
    }

    public SysOfficeDto getSysOffice(String officeCode) {
        TsysOffice sysOffice = sysOfficeBusiness.getById(officeCode);
        SysOfficeDto sysOfficeDto = new SysOfficeDto();
        BeanUtil.copyProperties(sysOfficeDto, sysOffice);
        return sysOfficeDto;
    }

    public SysOfficeDto addSysOffice(SysOfficeDto sysOfficeDto) {
        TsysOffice sysOffice = BeanUtil.copyProperties(TsysOffice.class, sysOfficeDto);
        return BeanUtil.copyProperties(SysOfficeDto.class, sysOfficeBusiness.add(sysOffice));
    }

    public int updateSysOffice(SysOfficeDto sysOfficeDto) {
        TsysOffice sysOffice = BeanUtil.copyProperties(TsysOffice.class, sysOfficeDto);
        return sysOfficeBusiness.update(sysOffice);
    }

    public void deleteSysOffices(String[] ids) {
        sysOfficeBusiness.deleteByKeys(ids);
    }

    public PageResponse getSysOfficePager(PageRequest pageRequest,
                                          SysOfficeDto sysOfficeDto) {
        TsysOffice sysOffice = BeanUtil.copyProperties(TsysOffice.class, sysOfficeDto);
        String sortField = pageRequest.getSort();
        if (StringUtil.isEmpty(sortField)) {
            sortField = CamelCaseUtil.getFieldName("officeCode");
        }
        String orderByField = CamelCaseUtil.getFieldName(sortField);
        OrderBy orderBy = new OrderBy(orderByField, "asc".equalsIgnoreCase(pageRequest.getOrder()));
        Pager<TsysOffice> rolePager = sysOfficeBusiness.getPager(pageRequest.getStart(), pageRequest.getPageSize(), sysOffice, orderBy);
        return PageResponseAdapter.transform(rolePager);
    }

    public boolean checkSysOfficeExists(SysOfficeDto sysOfficeDto) {
        TsysOffice sysOffice = BeanUtil.copyProperties(TsysOffice.class, sysOfficeDto);
        return sysOfficeBusiness.checkExists(sysOffice);
    }

    public List getOfficeTree(TreeDto treeDto) {
        TreeData tree = BeanUtil.copyProperties(TreeData.class, treeDto);
        return sysOfficeBusiness.getOfficeTree(tree);
    }

    public List getOfficeTreeByDep(TreeDto treeDto) {
        String currentOfficeCode = treeDto.getId();
        treeDto.setId(null);
        TreeData tree = BeanUtil.copyProperties(TreeData.class, treeDto);
        List<TreeData> treeList = sysOfficeBusiness.getOfficeTreeByDep(tree);
        for (TreeData treeData : treeList) {
            String officeCode = treeData.getId();
            //为当前节点设置disableClick、disableSub
            if (officeCode.equals(currentOfficeCode)) {
                treeData.setDisableClick("true");
                treeData.setDisableSub("true");
                break;
            }
        }
        return treeList;
    }

    /**
     * TODO
     *
     * @param sysOfficeDto
     * @return
     */
    public List getOfficeList(SysOfficeDto sysOfficeDto) {
        if (sysOfficeDto == null) {
            sysOfficeDto = new SysOfficeDto();
        }
        TsysOffice office = BeanUtil.copyProperties(TsysOffice.class, sysOfficeDto);
        return sysOfficeBusiness.getOfficeList(office);
    }

	/*public List getOfficesTree(SysOfficeDto sysOfficeDto) {
        TsysOffice tsysOffice = BeanUtil.copyProperties(TsysOffice.class,sysOfficeDto);
		return sysOfficeBusiness.getOfficeTree(tsysOffice);
	}*/


}
