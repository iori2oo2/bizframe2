package org.tinygroup.bizframe.business.impl;

import java.util.*;

import org.tinygroup.bizframe.business.inter.SysMenuBusiness;
import org.tinygroup.bizframe.dao.inter.TsysMenuDao;
import org.tinygroup.bizframe.dao.inter.pojo.TreeData;
import org.tinygroup.bizframe.dao.inter.pojo.TsysMenu;
import org.tinygroup.jdbctemplatedslsession.daosupport.OrderBy;
import org.tinygroup.tinysqldsl.Pager;

public class SysMenuBusinessImpl implements SysMenuBusiness {
	private TsysMenuDao tsysMenuDao;
	

	public TsysMenuDao getTsysMenuDao() {
		return tsysMenuDao;
	}

	public void setTsysMenuDao(TsysMenuDao tsysMenuDao) {
		this.tsysMenuDao = tsysMenuDao;
	}

	public TsysMenu getById(Integer id) {
		return tsysMenuDao.getByKey(id);
	}

	public Pager<TsysMenu> getPager(int start, int limit,
			TsysMenu sysMenu, OrderBy... orderBies) {
		return tsysMenuDao.queryPagerForSearch(start, limit, sysMenu, orderBies);
	}

	public TsysMenu add(TsysMenu sysMenu) {
		return tsysMenuDao.add(sysMenu);
	}

	public int update(TsysMenu sysMenu) {
		return tsysMenuDao.edit(sysMenu);
	}

	public int deleteByKeys(Integer... pks) {
		return tsysMenuDao.deleteByKeys(pks);
	}

	public boolean checkExists(TsysMenu sysMenu) {
		return tsysMenuDao.checkExist(sysMenu).size() == 0 ? false : true;
	}

	public List<TsysMenu> getMenuInfos(TsysMenu sysMenu) {
		return tsysMenuDao.query(sysMenu);
	}

	public List<TsysMenu> getMenuTree(TreeData tree) {
		return tsysMenuDao.getMenuTree(tree);
	}

	public List<TsysMenu> getSysMenuList(TsysMenu tsysMenu) {
		return tsysMenuDao.query(tsysMenu);
	}

	public List<Integer> findMenuIdsByUser(String userCode, OrderBy... orderArgs) {
//		OrderBy orderBy = new OrderBy("menu_id",true);
		return tsysMenuDao.findMenuIdsByUser(userCode);
	}

}
